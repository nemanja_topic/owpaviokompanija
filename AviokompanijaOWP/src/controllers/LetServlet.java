package controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.AerodromDAO;
import dao.LetDAO;
import dao.SedisteDAO;
import model.Korisnik;
import model.Let;
import model.Sediste;

public class LetServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		Let let = new Let();
		try{
			let = LetDAO.get(Integer.parseInt(id));
			int i = 0;
			for (Sediste s : SedisteDAO.getAll(let.getId())) {
				if(!s.isZauzeto()) {
					i++;
				}
			}
			let.setSvaSedista(i);
		} catch(Exception e) {
			
		}
		
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		Map<String, Object> data = new HashMap<>();
		
		if(ulogovaniKorisnik == null) {
			data.put("role", "NEPRIJAVLJEN");
		} else if(ulogovaniKorisnik != null) {
			if(ulogovaniKorisnik.getUloga().toString() == "PRIJAVLJEN") {
				data.put("role", "PRIJAVLJEN");
			}else if(ulogovaniKorisnik.getUloga().toString() == "ADMINISTRATOR") {
				data.put("role", "ADMINISTRATOR");
			}
		}
		
		data.put("status", "success");
		data.put("let", let);

		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		mapper.setDateFormat(df);
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if(ulogovaniKorisnik.getUloga().toString() != "ADMINISTRATOR") {
			return;
		}
		
		String id = request.getParameter("id");
		String idPolazista = request.getParameter("polaziste");
		String idDestinacije = request.getParameter("destinacija");
		String cenaKarte = request.getParameter("cenaKarte");
		String option = request.getParameter("option");
		String message = "Let je uspesno izmenjen";
		
		Let let = new Let();
		try{
			let = LetDAO.get(Integer.parseInt(id));
		} catch(Exception e) {
			message = "Ne postoji let sa tim ID-em!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		
		if(option.equals("DELETE")) {
			LetDAO.delete(let.getId());
			
			Map<String, Object> data = new HashMap<>();
			data.put("status", "success");
			data.put("message", "Let je uspesno obrisan!");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			response.sendRedirect("index.html");
			return;
		}
		if(option.equals("EDIT")) {
			try {
				let.setPolaziste(AerodromDAO.getById(Integer.parseInt(idPolazista)));
			}catch(Exception e) {
				message = "Greska prilikom odabira polazista!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			try {
				let.setDestinacija(AerodromDAO.getById(Integer.parseInt(idDestinacije)));
			}catch(Exception e) {
				message = "Ne postoji let sa tim ID-em!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			try {
				let.setCenaKarte(Double.parseDouble(cenaKarte));
			}catch(Exception e) {
				message = "Cena karte mora biti broj!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			if(let.getPolaziste().getId() == let.getDestinacija().getId()) {
				message = "Ne mozete putovati sa istog mjesta na isto mjesto!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			LetDAO.update(let);
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "success");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
	}
}
