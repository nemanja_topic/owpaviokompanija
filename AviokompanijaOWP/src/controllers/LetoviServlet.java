package controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.LetDAO;
import model.Korisnik;
import model.Let;

public class LetoviServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String brojFilter = request.getParameter("brojFilter");
		if (brojFilter == null)
			brojFilter = "";
		String polazisteFilter = request.getParameter("polazisteFilter");
		if (polazisteFilter == null)
			polazisteFilter = "";
		String destinacijaFilter = request.getParameter("destinacijaFilter");
		if (destinacijaFilter == null)
			destinacijaFilter = "";
		double cenaLowFilter = 0;
		try {
			cenaLowFilter = Double.parseDouble(request.getParameter("cenaLowFilter"));		
		} catch (Exception ex) {}
		double cenaHighFilter = Double.MAX_VALUE;
		try {
			cenaHighFilter = Double.parseDouble(request.getParameter("cenaHighFilter"));		
		} catch (Exception ex) {}
		Date vremePolaskaLowFilter = new Date();
		try {
			vremePolaskaLowFilter = new SimpleDateFormat("yyyyMMdd").parse("19900101");
			  try { vremePolaskaLowFilter = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(request.getParameter("vremePolaskaLowFilter")); 
			  } catch (Exception ex) {} 
		} catch (Exception ex) {}
		Date vremePolaskaHighFilter = new Date();
		try {
			vremePolaskaHighFilter = new SimpleDateFormat("yyyyMMdd").parse("21001212");
			try { vremePolaskaHighFilter = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(request.getParameter("vremePolaskaHighFilter")); 
			  } catch (Exception ex) {} 
		} catch (Exception ex) {}
		Date vremeDolaskaLowFilter = new Date();
		try {
			vremeDolaskaLowFilter = new SimpleDateFormat("yyyyMMdd").parse("19900101");
			try { vremeDolaskaLowFilter = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(request.getParameter("vremeDolaskaLowFilter")); 
			  } catch (Exception ex) {}
		} catch (Exception ex) {}
		Date vremeDolaskaHighFilter = new Date();
		try {
			vremeDolaskaHighFilter = new SimpleDateFormat("yyyyMMdd").parse("21001212");
			try { vremeDolaskaHighFilter = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(request.getParameter("vremeDolaskaHighFilter")); 
			  } catch (Exception ex) {}
		} catch (Exception ex) {}
		
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		List<Let> filteredLetovi = LetDAO.getAll(brojFilter, polazisteFilter, destinacijaFilter, cenaLowFilter, cenaHighFilter, vremePolaskaLowFilter, vremePolaskaHighFilter, vremeDolaskaLowFilter, vremeDolaskaHighFilter);

		Map<String, Object> data = new HashMap<>();
		data.put("status", "success");
		if(ulogovaniKorisnik != null) {
			data.put("role", ulogovaniKorisnik.getUloga().toString());
			data.put("ulogovaniKorisnik", ulogovaniKorisnik);
		}else {
			data.put("role", "NEPRIJAVLJEN");
		}
		data.put("letovi", filteredLetovi);

		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		mapper.setDateFormat(df);
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
