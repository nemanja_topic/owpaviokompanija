package model;

import java.util.Date;

public class Korisnik {
	private int id;
	private String korisnickoIme;
	private String lozinka;
	private Date datumRegistracije;
	private Role uloga;
	private boolean blokiran;
	private boolean obrisan;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public Date getDatumRegistracije() {
		return datumRegistracije;
	}
	public void setDatumRegistracije(Date datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}
	public Role getUloga() {
		return uloga;
	}
	public void setUloga(Role uloga) {
		this.uloga = uloga;
	}
	public boolean isBlokiran() {
		return blokiran;
	}
	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}	
	public boolean isObrisan() {
		return obrisan;
	}
	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}
	
	public Korisnik() {
		super();
	}
	
	public Korisnik(int id, String korisnickoIme, String lozinka, Date datumRegistracije, Role uloga,
			boolean blokiran, boolean obrisan) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.datumRegistracije = datumRegistracije;
		this.uloga = uloga;
		this.blokiran = blokiran;
		this.obrisan = obrisan;
	}
	
	@Override
	public String toString() {
		return "Korisnik [Id=" + id + ", korisnickoIme=" + korisnickoIme + ", lozinka=" + lozinka
				+ ", datumRegistracije=" + datumRegistracije + ", uloga=" + uloga + ", blokiran=" + blokiran + ", obrisan=" + obrisan + "]";
	}
}
