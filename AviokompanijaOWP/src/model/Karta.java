package model;

import java.util.Date;

public class Karta {
	private int id;
	private Let polazniLet;
	private Let povratniLet;
	private Sediste polaznoSediste;
	private Sediste povratnoSediste;
	private Date vremeRezervacije;
	private Date vremeProdaje;
	private Korisnik korisnik;
	private String imePutnika;
	private String prezimePutnika;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Let getPolazniLet() {
		return polazniLet;
	}
	public void setPolazniLet(Let polazniLet) {
		this.polazniLet = polazniLet;
	}
	public Let getPovratniLet() {
		return povratniLet;
	}
	public void setPovratniLet(Let povratniLet) {
		this.povratniLet = povratniLet;
	}
	public Sediste getPolaznoSediste() {
		return polaznoSediste;
	}
	public void setPolaznoSediste(Sediste polaznoSediste) {
		this.polaznoSediste = polaznoSediste;
	}
	public Sediste getPovratnoSediste() {
		return povratnoSediste;
	}
	public void setPovratnoSediste(Sediste povratnoSediste) {
		this.povratnoSediste = povratnoSediste;
	}
	public Date getVremeRezervacije() {
		return vremeRezervacije;
	}
	public void setVremeRezervacije(Date vremeRezervacije) {
		this.vremeRezervacije = vremeRezervacije;
	}
	public Date getVremeProdaje() {
		return vremeProdaje;
	}
	public void setVremeProdaje(Date vremeProdaje) {
		this.vremeProdaje = vremeProdaje;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public String getImePutnika() {
		return imePutnika;
	}
	public void setImePutnika(String imePutnika) {
		this.imePutnika = imePutnika;
	}
	public String getPrezimePutnika() {
		return prezimePutnika;
	}
	public void setPrezimePutnika(String prezimePutnika) {
		this.prezimePutnika = prezimePutnika;
	}
	
	public Karta() {
		super();
	}
	
	public Karta(int id, Let polazniLet, Let povratniLet, Sediste polaznoSediste, Sediste povratnoSediste,
			Date vremeRezervacije, Date vremeProdaje, Korisnik korisnik, String imePutnika, String prezimePutnika) {
		super();
		this.id = id;
		this.polazniLet = polazniLet;
		this.povratniLet = povratniLet;
		this.polaznoSediste = polaznoSediste;
		this.povratnoSediste = povratnoSediste;
		this.vremeRezervacije = vremeRezervacije;
		this.vremeProdaje = vremeProdaje;
		this.korisnik = korisnik;
		this.imePutnika = imePutnika;
		this.prezimePutnika = prezimePutnika;
	}
	
	@Override
	public String toString() {
		return "Karta [id=" + id + ", polazniLet=" + polazniLet + ", povratniLet=" + povratniLet + ", polaznoSediste="
				+ polaznoSediste + ", povratnoSediste=" + povratnoSediste + ", vremeRezervacije=" + vremeRezervacije
				+ ", vremeProdaje=" + vremeProdaje + ", korisnik=" + korisnik + ", imePutnika=" + imePutnika
				+ ", prezimePutnika=" + prezimePutnika + "]";
	}
}
