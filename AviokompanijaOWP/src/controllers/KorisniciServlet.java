package controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KorisnikDAO;
import model.Korisnik;

public class KorisniciServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if(ulogovaniKorisnik == null) {
			return;
		}
		
		String korisnickoImeFilter = request.getParameter("korisnickoImeFilter");
		if (korisnickoImeFilter == null)
			korisnickoImeFilter = "";
		String ulogaFilter = request.getParameter("ulogaFilter");
		if (ulogaFilter == null)
			ulogaFilter = "";

		List<Korisnik> filteredKorisnici = KorisnikDAO.getAll(korisnickoImeFilter, ulogaFilter);

		Map<String, Object> data = new HashMap<>();
		data.put("status", "success");
		if(ulogovaniKorisnik.getUloga().toString() == "ADMINISTRATOR") {
			data.put("role", ulogovaniKorisnik.getUloga().toString());
			data.put("ulogovaniKorisnik", ulogovaniKorisnik);
		}
		data.put("korisnici", filteredKorisnici);

		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		mapper.setDateFormat(df);
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
