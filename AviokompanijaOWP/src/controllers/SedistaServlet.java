package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.SedisteDAO;
import model.Sediste;

public class SedistaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idLeta = request.getParameter("idLeta");
		String option = request.getParameter("slobodnaSedista");
		List<Sediste> sedista = SedisteDAO.getAll(Integer.parseInt(idLeta));

		if (option.equals("slobodnaSedista")) {
			sedista.clear();
			for (Sediste s : SedisteDAO.getAll(Integer.parseInt(idLeta))) {
				if (!s.isZauzeto()) {
					sedista.add(s);
				}
			}
		}

		Map<String, Object> data = new HashMap<>();
		data.put("status", "success");
		data.put("sedista", sedista);

		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
