package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Aerodrom;
import model.Karta;
import model.Let;
import model.Sediste;

public class KartaDAO {

	public static Karta get(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika "
					+ "FROM karte WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			if (rset.next()) {
				int index = 1;
				int idKarte = rset.getInt(index++);
				int polazniLet_id = rset.getInt(index++);
				int povratniLet_id = rset.getInt(index++);
				int polaznoSediste_id = rset.getInt(index++);
				int povratnoSediste_id = rset.getInt(index++);
				Date vremeRezervacije = rset.getTimestamp(index++);
				Date vremeProdaje = rset.getTimestamp(index++);
				int korisnikId = rset.getInt(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				
				Let povratniLet = null;
				Sediste povratnoSediste = null;
				if(povratniLet_id != 0) {
					povratniLet = LetDAO.get(povratniLet_id);
					povratnoSediste = SedisteDAO.get(povratnoSediste_id);
				}else {
					Aerodrom aerodrom = new Aerodrom(-1, "");
					povratniLet = new Let(-1, "", new Date(), new Date(), aerodrom, aerodrom, -1, -1, false);
					povratnoSediste = new Sediste(-1, povratniLet, -1, false);
				}

				return new Karta(idKarte, LetDAO.get(polazniLet_id), povratniLet, SedisteDAO.get(polaznoSediste_id),
						povratnoSediste, vremeRezervacije, vremeProdaje, KorisnikDAO.get(korisnikId), imePutnika,
						prezimePutnika);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}

		return null;
	}

	public static List<Karta> getAll() {
		ArrayList<Karta> karte = new ArrayList<Karta>();
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika FROM karte";

			pstmt = conn.prepareStatement(query);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			while (rset.next()) {
				int index = 1;
				int idKarte = rset.getInt(index++);
				int polazniLet_id = rset.getInt(index++);
				int povratniLet_id = rset.getInt(index++);
				int polaznoSediste_id = rset.getInt(index++);
				int povratnoSediste_id = rset.getInt(index++);
				Date vremeRezervacije = rset.getTimestamp(index++);
				Date vremeProdaje = rset.getTimestamp(index++);
				int korisnikId = rset.getInt(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				
				Let povratniLet = null;
				if(povratniLet_id != 0) {
					povratniLet = LetDAO.get(povratniLet_id);
				}else {
					Aerodrom aerodrom = new Aerodrom(-1, "");
					povratniLet = new Let(-1, "", new Date(), new Date(), aerodrom, aerodrom, -1, -1, false);
				}

				Karta karta = new Karta(idKarte, LetDAO.get(polazniLet_id), povratniLet, SedisteDAO.get(polaznoSediste_id),
						SedisteDAO.get(povratnoSediste_id), vremeRezervacije, vremeProdaje, KorisnikDAO.get(korisnikId), imePutnika,
						prezimePutnika);

				karte.add(karta);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}

		return karte;
	}
	
	/*
	 * public static List<Karta> getAllForLet() { ArrayList<Karta> karte = new
	 * ArrayList<Karta>(); Connection conn = ConnectionManager.getConnection();
	 * 
	 * PreparedStatement pstmt = null; ResultSet rset = null; try { String query =
	 * "SELECT id, polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika FROM karte"
	 * ;
	 * 
	 * pstmt = conn.prepareStatement(query); System.out.println(pstmt);
	 * 
	 * rset = pstmt.executeQuery(); while (rset.next()) { int index = 1; int idKarte
	 * = rset.getInt(index++); int polazniLet_id = rset.getInt(index++); int
	 * povratniLet_id = rset.getInt(index++); int polaznoSediste =
	 * rset.getInt(index++); int povratnoSediste = rset.getInt(index++); Date
	 * vremeRezervacije = rset.getDate(index++); Date vremeProdaje =
	 * rset.getDate(index++); int korisnikId = rset.getInt(index++); String
	 * imePutnika = rset.getString(index++); String prezimePutnika =
	 * rset.getString(index++);
	 * 
	 * Let povratniLet = null; if(povratniLet_id != 0) { povratniLet =
	 * LetDAO.get(povratniLet_id); }else { Aerodrom aerodrom = new Aerodrom(-1, "");
	 * povratniLet = new Let(-1, "", new Date(), new Date(), aerodrom, aerodrom, new
	 * ArrayList<>(), new ArrayList<>(), -1, false); }
	 * 
	 * Karta karta = new Karta(idKarte, LetDAO.get(polazniLet_id), povratniLet,
	 * polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje,
	 * KorisnikDAO.get(korisnikId), imePutnika, prezimePutnika);
	 * 
	 * karte.add(karta); } } catch (SQLException ex) {
	 * System.out.println("Greska u SQL upitu!"); ex.printStackTrace(); } finally {
	 * try { pstmt.close(); } catch (SQLException ex1) { ex1.printStackTrace(); }
	 * try { rset.close(); } catch (SQLException ex1) { ex1.printStackTrace(); } }
	 * 
	 * return karte; }
	 */

	public static boolean add(Karta karta) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, karta.getPolazniLet().getId());
			if (karta.getPovratniLet() != null) {
				pstmt.setInt(index++, karta.getPovratniLet().getId());
			} else {
				pstmt.setInt(index++, -1);
			}
			pstmt.setInt(index++, karta.getPolaznoSediste().getId());
			if (karta.getPovratniLet() != null) {
				pstmt.setInt(index++, karta.getPovratnoSediste().getId());
			} else {
				pstmt.setInt(index++, -1);
			}
			if (karta.getVremeRezervacije() != null) {
				pstmt.setTimestamp(index++, new java.sql.Timestamp(karta.getVremeRezervacije().getTime()));
			} else {
				pstmt.setTimestamp(index++, null);
			}
			if (karta.getVremeProdaje() != null) {
				pstmt.setTimestamp(index++, new java.sql.Timestamp(karta.getVremeProdaje().getTime()));
			} else {
				pstmt.setTimestamp(index++, null);
			}
			pstmt.setInt(index++, karta.getKorisnik().getId());
			pstmt.setString(index++, karta.getImePutnika());
			pstmt.setString(index++, karta.getPrezimePutnika());

			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}

	public static boolean update(Karta karta) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE karte SET vremeRezervacije = ?, vremeProdaje = ?, ime_putnika = ?, prezime_putnika = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			if (karta.getVremeProdaje() == null) {
				pstmt.setTimestamp(index++, new java.sql.Timestamp(karta.getVremeRezervacije().getTime()));
				pstmt.setTimestamp(index++, null);
			}else {
				pstmt.setTimestamp(index++, null);
				pstmt.setTimestamp(index++, new java.sql.Timestamp(karta.getVremeProdaje().getTime()));
			}
			pstmt.setString(index++, karta.getImePutnika());
			pstmt.setString(index++, karta.getPrezimePutnika());
			pstmt.setInt(index++, karta.getId());

			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}

	public static boolean delete(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM karte WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);

			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}

}
