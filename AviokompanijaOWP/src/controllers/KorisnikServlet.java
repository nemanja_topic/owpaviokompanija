package controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KorisnikDAO;
import model.Korisnik;
import model.Role;

public class KorisnikServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik korisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		Map<String, Object> data = new HashMap<>();
		if (korisnik == null) {
			data.put("status", "unauthenticated");
			data.put("role", "NEPRIJAVLJEN");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		if (korisnik.getUloga().toString() == "PRIJAVLJEN") {
			data.put("status", "success");
			data.put("role", korisnik.getUloga().toString());
			data.put("ulogovanKorisnik", korisnik);
		}
		if (korisnik.getUloga().toString() == "ADMINISTRATOR") {
			data.put("status", "success");
			data.put("role", korisnik.getUloga().toString());
			data.put("ulogovanKorisnik", korisnik);
			String id = request.getParameter("id");
			System.out.println(id);
			Korisnik korisnik2 = new Korisnik();
			try {
				korisnik2 = KorisnikDAO.get(Integer.parseInt(id));
			} catch (Exception e) {

			}
			data.put("korisnik2", korisnik2);
		}

		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		mapper.setDateFormat(df);
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
		return;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if (ulogovaniKorisnik.getUloga().toString() == "NEPRIJAVLJEN" || ulogovaniKorisnik.isBlokiran()) {
			return;
		}

		String id = request.getParameter("id");
		String korisnickoIme = request.getParameter("korisnickoIme");
		String lozinka = request.getParameter("lozinka");
		String message = "Uspesno";

		if (ulogovaniKorisnik.getUloga().toString() == "PRIJAVLJEN") {
			Korisnik korisnik = new Korisnik();
			try {
				korisnik = KorisnikDAO.get(Integer.parseInt(id));
			} catch (Exception e) {
				message = "Ne postoji korisnik sa tim ID-em!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}

			boolean postoji = false;
			for (Korisnik k : KorisnikDAO.getAll()) {
				if (k.getKorisnickoIme().equals(korisnickoIme)) {
					postoji = true;
				}
			}
			if (postoji == false) {
				korisnik.setKorisnickoIme(korisnickoIme);
				ulogovaniKorisnik.setKorisnickoIme(korisnickoIme);
			}
			
			if (lozinka != null && lozinka != "") {
				korisnik.setLozinka(lozinka);
			}
			KorisnikDAO.update(korisnik);
		}

		if (ulogovaniKorisnik.getUloga().toString() == "ADMINISTRATOR") {
			String blokiran = request.getParameter("isBlokiran");
			String isAdmin = request.getParameter("isAdmin");
			String option = request.getParameter("option");

			Korisnik korisnik = new Korisnik();
			try {
				korisnik = KorisnikDAO.get(Integer.parseInt(id));
			} catch (Exception e) {
				message = "Ne postoji korisnik sa tim ID-em!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}

			if (option.equals("DELETE")) {
				KorisnikDAO.delete(korisnik.getId());
				// response.sendRedirect("Korisnici.html");

				Map<String, Object> data = new HashMap<>();
				data.put("status", "success");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
			}
			if (option.equals("EDIT")) {
				
				
				boolean postoji = false;
				for (Korisnik k : KorisnikDAO.getAll()) {
					if (k.getKorisnickoIme().equals(korisnickoIme)) {
						postoji = true;
					}
				}
				if (postoji == false) {
					korisnik.setKorisnickoIme(korisnickoIme);
					ulogovaniKorisnik.setKorisnickoIme(korisnickoIme);
				}
				
				if (lozinka != null && lozinka != "") {
					korisnik.setLozinka(lozinka);
				}
				
				korisnik.setBlokiran(Boolean.valueOf(blokiran));
				if (isAdmin.equals("true")) {
					korisnik.setUloga(Role.ADMINISTRATOR);
				} else {
					korisnik.setUloga(Role.PRIJAVLJEN);
				}
				KorisnikDAO.update(korisnik);
			}
		}

	}
}
