DROP SCHEMA IF EXISTS aviokompanija;
CREATE SCHEMA aviokompanija DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE aviokompanija;

CREATE TABLE aerodromi (
	id INT NOT NULL AUTO_INCREMENT,
	naziv VARCHAR(25) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO aerodromi (naziv) VALUES ('Nikola Tesla Beograd');
INSERT INTO aerodromi (naziv) VALUES ('Los Angeles Airport');
INSERT INTO aerodromi (naziv) VALUES ('JFK Airport');
INSERT INTO aerodromi (naziv) VALUES ('London City Airport');
INSERT INTO aerodromi (naziv) VALUES ('Haneda Airport');

CREATE TABLE korisnici (
	id INT NOT NULL AUTO_INCREMENT,
	korisnickoIme VARCHAR(25) NOT NULL, 
	lozinka VARCHAR(25) NOT NULL,
    datumRegistracije DateTime NOT NULL,
	role ENUM('PRIJAVLJEN', 'ADMINISTRATOR') NOT NULL DEFAULT 'PRIJAVLJEN', 
	blokiran bool NOT NULL,
    obrisan bool NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO korisnici (korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan) VALUES ('a', 'a', '2019-02-01 15:00:00', 'ADMINISTRATOR', false, false);
INSERT INTO korisnici (korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan) VALUES ('b', 'b', '2019-02-02 20:00:00', 'PRIJAVLJEN', false, false);
INSERT INTO korisnici (korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan) VALUES ('c', 'c', '2019-02-03 12:00:00', 'PRIJAVLJEN', false, false);

CREATE TABLE letovi (
	id INT NOT NULL AUTO_INCREMENT, 
	broj VARCHAR(10) NOT NULL, 
    vremePolaska DATETIME NOT NULL,
    vremeDolaska DATETIME NOT NULL,
    polaziste_id INT NOT NULL,
    destinacija_id INT NOT NULL,
    brojSedista INT NOT NULL,
	cenaKarte DECIMAL(10, 2) NOT NULL DEFAULT 99999999.00, 
    obrisan bool NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(polaziste_id) REFERENCES aerodromi(id) ON DELETE RESTRICT, 
    FOREIGN KEY(destinacija_id) REFERENCES aerodromi(id) ON DELETE RESTRICT 
);

INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('100', '2018-03-05 15:00:00', '2018-03-06 12:00:00', 1, 2, 5, 15000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('101', '2018-03-07 16:00:00', '2018-03-08 13:00:00', 1, 4, 6, 17000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('102', '2018-03-09 17:00:00', '2018-03-10 14:00:00', 2, 3, 4, 18000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('103', '2018-03-11 18:00:00', '2018-03-12 15:00:00', 4, 3, 7, 19000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('104', '2018-03-13 19:00:00', '2018-03-14 16:00:00', 5, 4, 8, 12000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('105', '2019-08-05 15:00:00', '2019-08-06 12:00:00', 1, 4, 5, 13000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('106', '2019-08-07 16:00:00', '2019-08-08 13:00:00', 1, 5, 4, 14000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('107', '2019-08-09 17:00:00', '2019-08-10 14:00:00', 4, 2, 5, 10000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('108', '2019-08-11 18:00:00', '2019-08-12 15:00:00', 3, 2, 4, 20000, false);
INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan)VALUES ('109', '2019-08-13 19:00:00', '2019-08-14 16:00:00', 1, 2, 5, 21000, false);

CREATE TABLE sedista (
	id INT NOT NULL AUTO_INCREMENT,
	let_id INT NOT NULL,
	broj INT NOT NULL,
    zauzeto bool NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(let_id) REFERENCES letovi(id) ON DELETE CASCADE
);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (1, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (1, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (1, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (1, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (1, 5, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (2, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (2, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (2, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (2, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (2, 5, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (2, 6, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (3, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (3, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (3, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (3, 4, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 5, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 6, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (4, 7, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 5, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 6, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 7, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (5, 8, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (6, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (6, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (6, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (6, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (6, 5, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (7, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (7, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (7, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (7, 4, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (8, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (8, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (8, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (8, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (8, 5, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (9, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (9, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (9, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (9, 4, false);

INSERT INTO sedista (let_id, broj, zauzezo) VALUES (10, 1, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (10, 2, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (10, 3, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (10, 4, false);
INSERT INTO sedista (let_id, broj, zauzezo) VALUES (10, 5, false);

CREATE TABLE karte (
	id INT NOT NULL AUTO_INCREMENT,
    polazniLet_id INT NOT NULL,
    povratniLet_id INT,
	polaznoSediste INT NOT NULL,
    povratnoSediste INT,
    vremeRezervacije DATETIME,
    vremeProdaje DATETIME,
    korisnik_id INT NOT NULL,
    ime_putnika VARCHAR(20),
    prezime_putnika VARCHAR(20),
	PRIMARY KEY(id),
    FOREIGN KEY(polazniLet_id) REFERENCES letovi(id) ON DELETE RESTRICT, 
	FOREIGN KEY(povratniLet_id) REFERENCES letovi(id) ON DELETE RESTRICT, 
	FOREIGN KEY(korisnik_id) REFERENCES korisnici(id) ON DELETE RESTRICT
);

INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) 
			VALUES (1, 1, 1, 1, '2019-03-13 19:00:00', null, 2, 'Nemanja', 'Topic');
INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) 
			VALUES (2, 1, 1, 1, '2019-03-13 19:00:00', null, 2, 'Jova', 'Jovic');
INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) 
			VALUES (1, 2, 1, 1, '2019-03-13 19:00:00', null, 3, 'Nemanja', 'Topic');

INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) 
			VALUES (3, null, 1, 1, null, '2019-03-13 19:00:00', 2, 'Pera', 'Peric');
INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) 
			VALUES (1, null, 1, 1, null, '2019-03-13 19:00:00', 3, 'Nemanja', 'Topic');
INSERT INTO karte (polazniLet_id, povratniLet_id, polaznoSediste, povratnoSediste, vremeRezervacije, vremeProdaje, korisnik_id, ime_putnika, prezime_putnika) 
			VALUES (2, null, 1, 1, null, '2019-03-13 19:00:00', 3, 'Sara', 'Saric');