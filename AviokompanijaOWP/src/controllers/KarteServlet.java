package controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KartaDAO;
import model.Karta;
import model.Korisnik;

public class KarteServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if(ulogovaniKorisnik == null) {
			Map<String, Object> data = new HashMap<>();
			data.put("role", "NEPRIJAVLJEN");
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		
		if(ulogovaniKorisnik.getUloga().toString() == "PRIJAVLJEN") {
			List<Karta> sveKarte = KartaDAO.getAll();
			List<Karta> prodate = new ArrayList<Karta>();
			List<Karta> rezervacije = new ArrayList<Karta>();
			
			for(Karta k : sveKarte) {
				if(k.getKorisnik().getId() == ulogovaniKorisnik.getId()) {
					if(k.getVremeProdaje() != null) {
						prodate.add(k);
					}
					if(k.getVremeRezervacije() != null) {
						rezervacije.add(k);
					}
				}
			}
			Map<String, Object> data = new HashMap<>();
			data.put("status", "success");
			data.put("role", ulogovaniKorisnik.getUloga().toString());
			data.put("ulogovaniKorisnik", ulogovaniKorisnik);
			
			data.put("prodateKarte", prodate);
			data.put("rezervisaneKarte", rezervacije);

			ObjectMapper mapper = new ObjectMapper();
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			mapper.setDateFormat(df);
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
		}
		
		if(ulogovaniKorisnik.getUloga().toString() == "ADMINISTRATOR") {
			List<Karta> sveKarte = KartaDAO.getAll();
			List<Karta> prodate = new ArrayList<Karta>();
			List<Karta> rezervacije = new ArrayList<Karta>();
			
			for(Karta k : sveKarte) {
				if(k.getVremeProdaje() != null) {
					prodate.add(k);
				}if(k.getVremeRezervacije() != null) {
					rezervacije.add(k);
				}
			}
			
			Map<String, Object> data = new HashMap<>();
			data.put("status", "success");
			data.put("role", ulogovaniKorisnik.getUloga().toString());
			data.put("ulogovaniKorisnik", ulogovaniKorisnik);
			
			data.put("prodateKarte", prodate);
			data.put("rezervisaneKarte", rezervacije);

			ObjectMapper mapper = new ObjectMapper();
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			mapper.setDateFormat(df);
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
