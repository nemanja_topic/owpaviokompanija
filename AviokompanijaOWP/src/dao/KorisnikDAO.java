package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Korisnik;
import model.Role;

public class KorisnikDAO {

	public static Korisnik get(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan "
					+ "FROM korisnici WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			if (rset.next()) {
				int index = 1;
				int idKorisnika = rset.getInt(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				Date datumRegistracije = rset.getTimestamp(index++);
				String role = rset.getString(index++);
				boolean blokiran = rset.getBoolean(index++);
				boolean obrisan = rset.getBoolean(index++);

				return new Korisnik(idKorisnika, korisnickoIme, lozinka, datumRegistracije, Role.valueOf(role),
						blokiran, obrisan);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return null;
	}

	public static List<Korisnik> getAll() {
		List<Korisnik> korisnici = new ArrayList<Korisnik>();
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan "
					+ "FROM korisnici";

			pstmt = conn.prepareStatement(query);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			while (rset.next()) {
				int index = 1;
				int idKorisnika = rset.getInt(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				Date datumRegistracije = rset.getTimestamp(index++);
				String role = rset.getString(index++);
				boolean blokiran = rset.getBoolean(index++);
				boolean obrisan = rset.getBoolean(index++);

				korisnici.add(new Korisnik(idKorisnika, korisnickoIme, lozinka, datumRegistracije, Role.valueOf(role),
						blokiran, obrisan));
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return korisnici;
	}

	public static List<Korisnik> getAll(String korisnickoImeFilter, String roleFilter) {
		List<Korisnik> korisnici = new ArrayList<>();

		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan "
					+ "FROM korisnici WHERE korisnickoIme LIKE ? AND role LIKE ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korisnickoImeFilter + "%");
			pstmt.setString(index++, "%" + roleFilter + "%");
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			while (rset.next()) {
				index = 1;
				int idKorisnika = rset.getInt(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				Date datumRegistracije = rset.getTimestamp(index++);
				String role = rset.getString(index++);
				boolean blokiran = rset.getBoolean(index++);
				boolean obrisan = rset.getBoolean(index++);

				korisnici.add(new Korisnik(idKorisnika, korisnickoIme, lozinka, datumRegistracije, Role.valueOf(role),
						blokiran, obrisan));
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return korisnici;
	}

	public static boolean add(Korisnik korisnik) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO korisnici (korisnickoIme, lozinka, datumRegistracije, role, blokiran, obrisan) VALUES (?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korisnik.getKorisnickoIme());
			pstmt.setString(index++, korisnik.getLozinka());
			pstmt.setTimestamp(index++, new java.sql.Timestamp(korisnik.getDatumRegistracije().getTime()));
			pstmt.setString(index++, korisnik.getUloga().toString());
			pstmt.setBoolean(index++, false);
			pstmt.setBoolean(index++, false);

			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}

	public static boolean update(Korisnik korisnik) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE korisnici SET korisnickoIme = ?, lozinka = ?, datumRegistracije = ?, role = ?, blokiran = ?, obrisan = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, korisnik.getKorisnickoIme());
			pstmt.setString(index++, korisnik.getLozinka());
			pstmt.setTimestamp(index++, new java.sql.Timestamp(korisnik.getDatumRegistracije().getTime()));
			pstmt.setString(index++, korisnik.getUloga().toString());
			pstmt.setBoolean(index++, korisnik.isBlokiran());
			pstmt.setBoolean(index++, korisnik.isObrisan());
			pstmt.setInt(index++, korisnik.getId());

			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}

	public static boolean delete(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM korisnici WHERE  id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
}
