package model;

public class Aerodrom {
	private int id;
	private String naziv;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public Aerodrom() {
		super();
	}
	
	public Aerodrom(int id, String naziv) {
		this.id = id;
		this.naziv = naziv;
	}
	
	@Override
	public String toString() {
		return "Aerodrom [Id=" + id + ", naziv=" + naziv + "]";
	}
}