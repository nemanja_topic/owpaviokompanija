package model;

public class Sediste {
	private int Id;
	private Let let;
	private int brojSedista;
	private boolean zauzeto;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public Let getLet() {
		return let;
	}
	public void setLet(Let let) {
		this.let = let;
	}
	public int getBrojSedista() {
		return brojSedista;
	}
	public void setBrojSedista(int brojSedista) {
		this.brojSedista = brojSedista;
	}
	public boolean isZauzeto() {
		return zauzeto;
	}
	public void setZauzeto(boolean zauzeto) {
		this.zauzeto = zauzeto;
	}
	
	public Sediste() {
		super();
	}
	public Sediste(int id, Let let, int brojSedista, boolean zauzeto) {
		super();
		Id = id;
		this.let = let;
		this.brojSedista = brojSedista;
		this.zauzeto = zauzeto;
	}
	@Override
	public String toString() {
		return "Sediste [Id=" + Id + ", brojSedista=" + brojSedista + ", zauzeto=" + zauzeto + "]";
	}
}
