package controllers;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KorisnikDAO;
import model.Korisnik;

public class RegisterServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String korisnickoIme = request.getParameter("korisnickoIme");
		String lozinka = request.getParameter("lozinka");
		String ponovljenaLozinka = request.getParameter("ponovljenaLozinka");

		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if(ulogovaniKorisnik != null) {
			Map<String, Object> data = new HashMap<>();
			data.put("status", "unauthenticated");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
//			response.sendRedirect("./Login.html");
			return;
		}
		
		String message = "Uspesno ste se registrovali!";
		String status = "success";
		String link = "<a href=\"Login.html\">Prijava</a>";
		try {
			if ("".equals(korisnickoIme) || "".equals(lozinka) || "".equals(ponovljenaLozinka))
				throw new Exception("Niste popunili sva polja!");
			if (!lozinka.equals(ponovljenaLozinka))
				throw new Exception("Lozinke se ne poklapaju!");

			Korisnik existingUser = null;
			for(Korisnik k : KorisnikDAO.getAll()) {
				if(k.getKorisnickoIme().equals(korisnickoIme)) {
					existingUser = k;
				}
			}
				
			if (existingUser != null)
				throw new Exception("Korisnik vec postoji!");
			
			Korisnik noviKorisnik = new Korisnik(1, korisnickoIme, lozinka, new Date(), model.Role.PRIJAVLJEN, false, false);
			KorisnikDAO.add(noviKorisnik);
		} catch (Exception ex) {
			message = ex.getMessage();
			status = "failure";
			link = "<a href=\"Register.html\">Povratak</a>";
		}

		request.setAttribute("title", "Registracija");
		request.setAttribute("message", message);
		request.setAttribute("link", link);
				
		Map<String, Object> data = new HashMap<>();
		data.put("message", message);
		data.put("status", status);

		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
	}
}
