package dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.cj.jdbc.MysqlDataSource;

public class ConnectionManager {
//	private static final String DATABASE = "localhost:3306/aviokompanija";
//	private static final String USER_NAME = "root";
//	private static final String PASSWORD = "root";	

	private static Connection connection;

	public static void open() {
		try {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setUser("root");
			dataSource.setPassword("root");
			dataSource.setUseSSL(false);
			dataSource.setUrl("jdbc:mysql://localhost:3306/aviokompanija");
			//dataSource.setServerName("com.mysql.jdbc.Driver");
			
			Class.forName("com.mysql.jdbc.Driver");
			connection = dataSource.getConnection();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	public static Connection getConnection() {
		return connection;
	}

	public static void close() {
		try {
			connection.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
}
