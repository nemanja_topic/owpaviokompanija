package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KorisnikDAO;
import model.Korisnik;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if (ulogovaniKorisnik != null) {
			Map<String, Object> data = new HashMap<>();
			data.put("status", "unauthenticated");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			response.sendRedirect("./index.html");
			return;
		}

		String korisnickoIme = request.getParameter("korisnickoIme");
		String lozinka = request.getParameter("lozinka");

		String message = "Uspesna prijava!";
//		String link = "<a href=\"WebShopServlet\">Nastavak</a>";
		String status = "success";
		Korisnik korisnik = null;
		try {
			for (Korisnik k : KorisnikDAO.getAll()) {
				if (k.getKorisnickoIme().equals(korisnickoIme) && k.getLozinka().equals(lozinka)) {
					korisnik = k;
					break;
				}
			}
			if (korisnik == null)
				throw new Exception("Neispravno korisnicko ime i/ili lozinka!");

			session = request.getSession();
			korisnik.setLozinka("");
			session.setAttribute("ulogovaniKorisnik", korisnik);
		} catch (Exception ex) {
			message = ex.getMessage();
//			link = "<a href=\"Login.html\">Povratak</a>";
			status = "failure";
		}

		Map<String, Object> data = new HashMap<>();
		data.put("message", message);
		data.put("status", status);

		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
	}
}
