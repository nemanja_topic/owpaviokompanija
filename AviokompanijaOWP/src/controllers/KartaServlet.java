package controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KartaDAO;
import model.Karta;
import model.Korisnik;

public class KartaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik korisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		Map<String, Object> data = new HashMap<>();

		if (korisnik == null) {
			data.put("status", "unauthenticated");
			data.put("role", "NEPRIJAVLJEN");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}

		if (korisnik.getUloga().toString() == "PRIJAVLJEN") {
			data.put("status", "success");
			data.put("role", korisnik.getUloga().toString());
			data.put("ulogovanKorisnik", korisnik);
			String id = request.getParameter("id");
			System.out.println(id);
			Karta karta2 = new Karta();
			try {
				karta2 = KartaDAO.get(Integer.parseInt(id));
			} catch (Exception e) {

			}
			if (karta2.getKorisnik() != null & karta2.getKorisnik().getId() == korisnik.getId()) {
				data.put("karta2", karta2);
			}
		}
		if (korisnik.getUloga().toString() == "ADMINISTRATOR") {
			data.put("status", "success");
			data.put("role", korisnik.getUloga().toString());
			data.put("ulogovanKorisnik", korisnik);
			String id = request.getParameter("id");
			System.out.println(id);
			Karta karta2 = new Karta();
			try {
				karta2 = KartaDAO.get(Integer.parseInt(id));
			} catch (Exception e) {

			}
			System.out.println("Zasto nije dodao " + karta2);
			data.put("karta2", karta2);
		}

		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		mapper.setDateFormat(df);
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
		return;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if (ulogovaniKorisnik.getUloga().toString() == "NEPRIJAVLJEN" || ulogovaniKorisnik.isBlokiran()) {
			return;
		}

		String id = request.getParameter("id");
		String option = request.getParameter("option");

		Karta karta = new Karta();
		System.out.println(karta);
		try {
			karta = KartaDAO.get(Integer.parseInt(id));
			System.out.println(karta);
		} catch (Exception e) {

		}

		if (option.equals("DELETE")) {
			if (karta.getVremeProdaje() == null) {
				KartaDAO.delete(karta.getId());
				response.sendRedirect("Karte.html");

				Map<String, Object> data = new HashMap<>();
				data.put("status", "success");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
			} else {

			}
		}
		if (option.equals("EDIT")) {
			if (karta.getVremeProdaje() == null) {
				String ime = request.getParameter("ime");
				String prezime = request.getParameter("prezime");
				String isKupljeno = request.getParameter("isKupljeno");
				karta.setImePutnika(ime);
				karta.setPrezimePutnika(prezime);
				if (isKupljeno.equals("true")) {
					karta.setVremeRezervacije(null);
					karta.setVremeProdaje(new Date());
				}
				KartaDAO.update(karta);
				response.sendRedirect("Karte.html");

				Map<String, Object> data = new HashMap<>();
				data.put("status", "success");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
			} else {

			}
		}
	}
}
