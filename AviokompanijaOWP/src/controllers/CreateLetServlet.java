package controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.time.DateUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.AerodromDAO;
import dao.LetDAO;
import dao.SedisteDAO;
import model.Aerodrom;
import model.Korisnik;
import model.Let;
import model.Sediste;

public class CreateLetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");

		if (ulogovaniKorisnik.getUloga().toString() != "ADMINISTRATOR") {
			return;
		}

		Date datumPolaska = new Date();
		Date datumDolaska = new Date();
		Aerodrom polaziste = new Aerodrom();
		Aerodrom destinacija = new Aerodrom();
		int brojSedista = -1;
		double cenaKarte = -1;

		String brojLetaInput = request.getParameter("brojLeta");
		String datumPolaskaInput = request.getParameter("datumPolaska");
		String datumDolaskaInput = request.getParameter("datumDolaska");
		String polazisteInput = request.getParameter("polaziste");
		String destinacijaInput = request.getParameter("destinacija");
		String brojSedistaInput = request.getParameter("brojSedista");
		String cenaKarteInput = request.getParameter("cenaKarte");

		String message = "Let je uspesno dodat!";
		String status = "success";

		if (brojLetaInput.equals("") || brojLetaInput == null) {
			Map<String, Object> data = new HashMap<>();
			data.put("message", "Broj leta je prazan!");
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		try {
			datumPolaska = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(datumPolaskaInput);
			if (datumPolaska == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			message = "Pogresan format datuma polaska!";

			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		try {
			datumDolaska = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(datumDolaskaInput);
			if (datumDolaska == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			message = "Pogresan format datuma dolaska!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		try {
			polaziste = AerodromDAO.getById(Integer.parseInt(polazisteInput));
		} catch (Exception e) {
			message = "Greska prilikom odabira polazista!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		try {
			destinacija = AerodromDAO.getById(Integer.parseInt(destinacijaInput));
		} catch (Exception e) {
			message = "Greska prilikom odabira destinacije!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		try {
			brojSedista = Integer.parseInt(brojSedistaInput);
		} catch (Exception e) {
			message = "Broj sjedista mora biti integer!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		try {
			cenaKarte = Double.parseDouble(cenaKarteInput);
		} catch (Exception e) {
			message = "Cena karte mora biti broj!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}

		try {
			if (datumDolaska.before(datumPolaska)) {
				message = "Datum dolaska ne moze biti prije datuma polaska!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			if (datumPolaska.before(new Date())) {
				message = "Let mora kretati u buducnosti!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			if (polaziste.getId() == destinacija.getId()) {
				message = "Ne mozete putovati sa istog mjesta na isto mjesto!";
				Map<String, Object> data = new HashMap<>();
				data.put("message", message);
				data.put("status", "error");

				ObjectMapper mapper = new ObjectMapper();
				String jsonData = mapper.writeValueAsString(data);
				System.out.println(jsonData);

				response.setContentType("application/json");
				response.getWriter().write(jsonData);
				return;
			}
			for (Let l : LetDAO.getAll()) {
				if (l.getBroj() == brojLetaInput) {
					if (DateUtils.isSameDay(l.getVremePolaska(), datumPolaska)) {
						message = "Ne mogu postojati 2 leta koja imaju isti broj i isti datum polaska!";
						Map<String, Object> data = new HashMap<>();
						data.put("message", message);
						data.put("status", "error");

						ObjectMapper mapper = new ObjectMapper();
						String jsonData = mapper.writeValueAsString(data);
						System.out.println(jsonData);

						response.setContentType("application/json");
						response.getWriter().write(jsonData);
						return;
					}
				}
			}
			Let noviLet = new Let(1, brojLetaInput, datumPolaska, datumDolaska, polaziste, destinacija, brojSedista,
					cenaKarte, false);
			LetDAO.add(noviLet);
			Let poslednjiLet = LetDAO.getAll().get(LetDAO.getAll().size() - 1);
			for (int i = 1; i <= brojSedista; i++) {
				SedisteDAO.add(new Sediste(1, poslednjiLet, i, false));
			}
		} catch (Exception ex) {
			message = ex.getMessage();
			status = "failure";
		}

		request.setAttribute("title", "Registracija");
		request.setAttribute("message", message);

		Map<String, Object> data = new HashMap<>();
		data.put("message", message);
		data.put("status", status);

		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);

	}
}
