package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Aerodrom;
import model.Let;

public class LetDAO {

	public static Let get(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT letovi.id, letovi.broj, letovi.vremePolaska, letovi.vremeDolaska, "
					+ "A.id, A.naziv, B.id, B.naziv, letovi.brojSedista, letovi.cenaKarte, letovi.obrisan "
					+ "FROM letovi, aerodromi A, aerodromi B "
					+ "WHERE A.id = letovi.polaziste_id and B.id = letovi.destinacija_id and letovi.id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			if (rset.next()) {
				int index = 1;
				int idLeta = rset.getInt(index++);
				String brojLeta = rset.getString(index++);
				Date vremePolaska = rset.getTimestamp(index++);
				Date vremeDolaska = rset.getTimestamp(index++);
				int idPolazista = rset.getInt(index++);
				String nazivPolazista = rset.getString(index++);
				int idDestinacije = rset.getInt(index++);
				String nazivDestinacije = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cenaKarte = rset.getDouble(index++);
				boolean obrisan = rset.getBoolean(index++);
				
				Aerodrom polaziste = new Aerodrom(idPolazista, nazivPolazista);
				Aerodrom destinacija = new Aerodrom(idDestinacije, nazivDestinacije);
				
				return new Let(idLeta, brojLeta, vremePolaska, vremeDolaska, polaziste, destinacija, brojSedista, cenaKarte, obrisan);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return null;
	}
	
	public static List<Let> getAll() {
		ArrayList<Let> letovi = new ArrayList<Let>();
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT letovi.id, letovi.broj, letovi.vremePolaska, letovi.vremeDolaska, "
					+ "A.id, A.naziv, B.id, B.naziv, letovi.brojSedista, letovi.cenaKarte, letovi.obrisan "
					+ "FROM letovi, aerodromi A, aerodromi B "
					+ "WHERE A.id = letovi.polaziste_id and B.id = letovi.destinacija_id";

			pstmt = conn.prepareStatement(query);;
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			while (rset.next()) {
				int index = 1;
				int idLeta = rset.getInt(index++);
				String brojLeta = rset.getString(index++);
				Date vremePolaska = rset.getTimestamp(index++);
				Date vremeDolaska = rset.getTimestamp(index++);
				int idPolazista = rset.getInt(index++);
				String nazivPolazista = rset.getString(index++);
				int idDestinacije = rset.getInt(index++);
				String nazivDestinacije = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cenaKarte = rset.getDouble(index++);
				boolean obrisan = rset.getBoolean(index++);
				
				Aerodrom polaziste = new Aerodrom(idPolazista, nazivPolazista);
				Aerodrom destinacija = new Aerodrom(idDestinacije, nazivDestinacije);
				
				Let l = new Let(idLeta, brojLeta, vremePolaska, vremeDolaska, polaziste, destinacija, brojSedista, cenaKarte, obrisan);
				letovi.add(l);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return letovi;
	}
	
	public static List<Let> getAll(String brojFilter, String polazisteFilter, String destinacijaFilter, double cenaLowFilter, double cenaHighFilter, Date vremePolaskaLowFilter, Date vremePolaskaHighFilter, Date vremeDolaskaLowFilter, Date vremeDolaskaHighFilter) {
		List<Let> letovi = new ArrayList<>();
		
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT letovi.id, letovi.broj, letovi.vremePolaska, letovi.vremeDolaska, " + 
					"A.id, A.naziv, B.id, B.naziv, letovi.brojSedista, letovi.cenaKarte, letovi.obrisan " + 
					"FROM letovi, aerodromi A, aerodromi B " + 
					"WHERE A.id = letovi.polaziste_id AND B.id = letovi.destinacija_id " + 
					"AND letovi.broj LIKE ? AND A.naziv LIKE ? AND B.naziv LIKE ? AND letovi.cenaKarte >= ? AND letovi.cenaKarte <= ? " + 
					"AND letovi.vremePolaska BETWEEN ? AND ? AND letovi.vremeDolaska BETWEEN ? AND ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + brojFilter + "%");
			pstmt.setString(index++, "%" + polazisteFilter + "%");
			pstmt.setString(index++, "%" + destinacijaFilter + "%");
			pstmt.setDouble(index++, cenaLowFilter);
			pstmt.setDouble(index++, cenaHighFilter);
			pstmt.setTimestamp(index++, new java.sql.Timestamp(vremePolaskaLowFilter.getTime()));
			pstmt.setTimestamp(index++, new java.sql.Timestamp(vremePolaskaHighFilter.getTime()));
			pstmt.setTimestamp(index++, new java.sql.Timestamp(vremeDolaskaLowFilter.getTime()));
			pstmt.setTimestamp(index++, new java.sql.Timestamp(vremeDolaskaHighFilter.getTime()));
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			while (rset.next()) {
				index = 1;
				int idLeta = rset.getInt(index++);
				String brojLeta = rset.getString(index++);
				Date vremePolaska = rset.getTimestamp(index++);
				Date vremeDolaska = rset.getTimestamp(index++);
				int idPolazista = rset.getInt(index++);
				String nazivPolazista = rset.getString(index++);
				int idDestinacije = rset.getInt(index++);
				String nazivDestinacije = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cenaKarte = rset.getDouble(index++);
				boolean obrisan = rset.getBoolean(index++);
				
				Aerodrom polaziste = new Aerodrom(idPolazista, nazivPolazista);
				Aerodrom destinacija = new Aerodrom(idDestinacije, nazivDestinacije);
				
				List<Integer> svaSedista = new ArrayList<Integer>();
				svaSedista.add(brojSedista);
				
				List<Integer> slobodnaSedista = new ArrayList<Integer>();
				for(int i = 1; i <= svaSedista.get(0); i++) {
					slobodnaSedista.add(i);
				}
				
				Let l = new Let(idLeta, brojLeta, vremePolaska, vremeDolaska, polaziste, destinacija, brojSedista, cenaKarte, obrisan);
				letovi.add(l);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}	
		return letovi;
	}
	
	public static boolean add(Let let) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO letovi (broj, vremePolaska, vremeDolaska, polaziste_id, destinacija_id, brojSedista, cenaKarte, obrisan) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, let.getBroj());
			pstmt.setTimestamp(index++, new java.sql.Timestamp(let.getVremePolaska().getTime()));
			pstmt.setTimestamp(index++, new java.sql.Timestamp(let.getVremeDolaska().getTime()));
			pstmt.setInt(index++, let.getPolaziste().getId());
			pstmt.setInt(index++, let.getDestinacija().getId());
			pstmt.setInt(index++, let.getSvaSedista());
			pstmt.setDouble(index++, let.getCenaKarte());
			pstmt.setBoolean(index++, false);
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
	
	public static boolean update(Let let) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE letovi SET broj = ?, vremePolaska = ?, vremeDolaska = ?, polaziste_id = ?, destinacija_id = ?, brojSedista = ?, cenaKarte = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, let.getBroj());
			pstmt.setTimestamp(index++, new java.sql.Timestamp(let.getVremePolaska().getTime()));
			pstmt.setTimestamp(index++, new java.sql.Timestamp(let.getVremeDolaska().getTime()));
			pstmt.setInt(index++, let.getPolaziste().getId());
			pstmt.setInt(index++, let.getDestinacija().getId());
			pstmt.setInt(index++, let.getSvaSedista());
			pstmt.setDouble(index++, let.getCenaKarte());
			pstmt.setInt(index++, let.getId());
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
	
	public static boolean logicalDelete(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE obrisan = true FROM letovi WHERE  id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
	
	public static boolean delete(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM letovi WHERE  id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
}
