package model;

import java.util.Date;

public class Let {
	private int Id;
	private String broj;
	private Date vremePolaska;
	private Date vremeDolaska;
	private Aerodrom polaziste;
	private Aerodrom destinacija;
	private int svaSedista;
	private double cenaKarte;
	private boolean obrisan;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getBroj() {
		return broj;
	}
	public void setBroj(String broj) {
		this.broj = broj;
	}
	public Date getVremePolaska() {
		return vremePolaska;
	}
	public void setVremePolaska(Date vremePolaska) {
		this.vremePolaska = vremePolaska;
	}
	public Date getVremeDolaska() {
		return vremeDolaska;
	}
	public void setVremeDolaska(Date vremeDolaska) {
		this.vremeDolaska = vremeDolaska;
	}
	public Aerodrom getPolaziste() {
		return polaziste;
	}
	public void setPolaziste(Aerodrom polaziste) {
		this.polaziste = polaziste;
	}
	public Aerodrom getDestinacija() {
		return destinacija;
	}
	public void setDestinacija(Aerodrom destinacija) {
		this.destinacija = destinacija;
	}
	public int getSvaSedista() {
		return svaSedista;
	}
	public void setSvaSedista(int svaSedista) {
		this.svaSedista = svaSedista;
	}
	public double getCenaKarte() {
		return cenaKarte;
	}
	public void setCenaKarte(double cenaKarte) {
		this.cenaKarte = cenaKarte;
	}
	public boolean isObrisan() {
		return obrisan;
	}
	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}
	
	public Let() {
		super();
	}
	
	public Let(int id, String broj, Date vremePolaska, Date vremeDolaska, Aerodrom polaziste, Aerodrom destinacija,
			int svaSedista, double cenaKarte, boolean obrisan) {
		super();
		Id = id;
		this.broj = broj;
		this.vremePolaska = vremePolaska;
		this.vremeDolaska = vremeDolaska;
		this.polaziste = polaziste;
		this.destinacija = destinacija;
		this.svaSedista = svaSedista;
		this.cenaKarte = cenaKarte;
		this.obrisan = obrisan;
	}
	
	@Override
	public String toString() {
		return "Let [Id=" + Id + ", broj=" + broj + ", vremePolaska=" + vremePolaska + ", vremeDolaska=" + vremeDolaska
				+ ", polaziste=" + polaziste + ", destinacija=" + destinacija + ", svaSedista=" + svaSedista
				+ ", cenaKarte=" + cenaKarte + ", obrisan=" + obrisan + "]";
	}
}
