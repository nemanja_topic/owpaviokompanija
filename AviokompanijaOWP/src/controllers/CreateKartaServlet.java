package controllers;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.KartaDAO;
import dao.LetDAO;
import dao.SedisteDAO;
import model.Karta;
import model.Korisnik;
import model.Let;
import model.Sediste;

public class CreateKartaServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Korisnik ulogovaniKorisnik = (Korisnik) session.getAttribute("ulogovaniKorisnik");
		
		if(ulogovaniKorisnik.getUloga().toString() == "NEPRIJAVLJEN") {
			return;
		}
		
		Let polazniLet = new Let();
		Let povratniLet = new Let();
		Sediste polaznoSediste = new Sediste();
		Sediste povratnoSediste = new Sediste();
				
		String polazniLetInput = request.getParameter("polazniLet");
		try {
			if(polazniLetInput != null && !polazniLetInput.equals("")) {
				polazniLet = LetDAO.get(Integer.parseInt(polazniLetInput));
			}else {
				polazniLet = null;
			}
		}catch(Exception e) {
			
		}
		String povratniLetInput = request.getParameter("povratniLet");
		try {
			povratniLet = LetDAO.get(Integer.parseInt(povratniLetInput));
		}catch(Exception e) {
			String message = "Povratni let ne postoji iako nije obavezan ali meni puca aplikacija ako nema!";
			Map<String, Object> data = new HashMap<>();
			data.put("message", message);
			data.put("status", "error");

			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			System.out.println(jsonData);

			response.setContentType("application/json");
			response.getWriter().write(jsonData);
			return;
		}
		String polaznoSedisteInput = request.getParameter("polaznoSediste");
		try {
			polaznoSediste = SedisteDAO.get(Integer.parseInt(polaznoSedisteInput));
		}catch(Exception e) {
			
		}
		String povratnoSedisteInput = request.getParameter("povratnoSediste");
		try {
			if(povratnoSedisteInput != null && !povratnoSedisteInput.equals("")) {
				povratnoSediste = SedisteDAO.get(Integer.parseInt(povratnoSedisteInput));
			}else {
				povratnoSediste = null;
			}
			povratnoSediste = SedisteDAO.get(Integer.parseInt(povratnoSedisteInput));
		}catch(Exception e) {
			
		}
		String isKupljeno = request.getParameter("isKupljeno");
		
		String ime = request.getParameter("ime");
		String prezime = request.getParameter("prezime");
		//
		
		String message = "Let je uspesno dodat!";
		String status = "success";
		try {
			Karta novaKarta = new Karta();
			if(isKupljeno.equals("true")) {
				novaKarta = new Karta(1, polazniLet, povratniLet, polaznoSediste, povratnoSediste, null, new Date(), ulogovaniKorisnik, ime, prezime);
			}else{
				novaKarta = new Karta(1, polazniLet, povratniLet, polaznoSediste, povratnoSediste, new Date(), null, ulogovaniKorisnik, ime, prezime);
			}
			
			polaznoSediste.setZauzeto(true);
			SedisteDAO.update(polaznoSediste);
			povratnoSediste.setZauzeto(true);
			SedisteDAO.update(povratnoSediste);
			
			KartaDAO.add(novaKarta);
		} catch (Exception ex) {
			message = ex.getMessage();
			status = "failure";
		}

		request.setAttribute("title", "Registracija");
		request.setAttribute("message", message);
				
		Map<String, Object> data = new HashMap<>();
		data.put("message", message);
		data.put("status", status);

		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(data);
		System.out.println(jsonData);

		response.setContentType("application/json");
		response.getWriter().write(jsonData);
	}
}
