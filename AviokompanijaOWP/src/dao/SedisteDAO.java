package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Let;
import model.Sediste;

public class SedisteDAO {
	
	public static Sediste get(int id) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, let_id, broj, zauzeto FROM sedista WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			if (rset.next()) {
				int index = 1;
				int idSedista = rset.getInt(index++);
				int idLeta = rset.getInt(index++);
				int brojSedista = rset.getInt(index++);
				boolean zauzeto = rset.getBoolean(index++);
				
				return new Sediste(idSedista, LetDAO.get(idLeta), brojSedista, zauzeto);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return null;
	}
	
	public static List<Sediste> getAll(int idLeta) {
		ArrayList<Sediste> sedista = new ArrayList<Sediste>();
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT A.id, A.let_id, A.broj, A.zauzeto "
					+ "FROM sedista A, letovi B "
					+ "WHERE A.let_id = B.id and B.id = ?";

			pstmt = conn.prepareStatement(query);;
			pstmt.setInt(1, idLeta);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			while (rset.next()) {
				int index = 1;
				int idSedista = rset.getInt(index++);
				int let_id = rset.getInt(index++);
				int brojSedista = rset.getInt(index++);
				boolean zauzeto = rset.getBoolean(index++);
				
				Let let = LetDAO.get(let_id);
				
				Sediste s = new Sediste(idSedista, let, brojSedista, zauzeto);
				sedista.add(s);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return sedista;
	}
	
	public static boolean add(Sediste sediste) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO sedista(let_id, broj, zauzeto)\r\n" + 
					"SELECT max(letovi.id), ?, ? FROM letovi";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, sediste.getBrojSedista());
			pstmt.setBoolean(index++, sediste.isZauzeto());
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
	
	public static boolean update(Sediste sediste) {
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE sedista SET zauzeto = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setBoolean(index++, sediste.isZauzeto());
			pstmt.setInt(index++, sediste.getId());
			
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
}
